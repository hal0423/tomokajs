'use strict'

const mongoose = require('mongoose')
const { Info } = require('./models.js')
const { options } = require('../.config/db.js')

const debug = require('debug')('seed')


mongoose.connect('mongodb://127.0.0.1:50000', options('tmokenc'))
.then(async () => {
  debug('connected to the db')
  const info = [
  
    new Info({
      about: 'bot',
      values: {
        lastInfoUpdate: new Date
      }
    }),
    
    new Info({
      about: 'command',
      values: {
        totalUsed: 0,
        usedBy: [],
        usedOn: []
      }
    })
  
  ]
  


  info.forEach(async (v, i) => {
    await v.save()
    debug('saved ', i + 1)

    if (i == 1) {
      
        await mongoose.disconnect()
        debug('done')
    }
  })

    
})
.catch(debug)
/*
;[
  new Info({
    type: 'bot',
    info: {
      lastInfoUpdate: new Date
    }
  }),
  
  new Info({
    type: 'command',
    info: {
      totalUsed: 1,
      usedBy: [{
        user: '239825449637642240',
        count: 1
      }],
      usedOn: [{
        server: '418811018244784129',
        count: 1
      }]
    }
  })

].forEach(async (v, i) => {
  await v.save()
  debug('saved ' + i)
})

*/
