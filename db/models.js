'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const logg = {
  _id: false,
  status: Boolean,
  channelID: String // channel ID
}

exports.Server = mongoose.model('Server', new Schema({
  name: { type: String, required: true },
  id: { type: String, required: true },
  ownerID: String,
  
  roles: [{
    _id: false,
    roleID: String,
    name: String,
    mentionable: Boolean
  }],

  totalMember: Number,
  members: [{
    _id: false,
    userID: String,
    roleIDs: [String] //ID of role(s)
  }],

  roleAliases: [{
    _id: false,
    name: String,
    roles: [{
      _id: false,
      id: String,
      name: String,
      color: Number
    }],
    sort: { type: String, default: 'default' }
  }],
  
  setting: {
    _id: false,
    customPrefix: String,
    logging: logg,
    announce: {
      _id: false,
      userJoin: logg,
      userLeave: logg,
      userComeback: logg
    },
  },
  commandCount: {type: Number, default: 0},
  commandUsed: [String] 
}))

exports.Member = mongoose.model('Member', new Schema({
  name: String,
  oldName: [String],
  id: String,
  tag: String,
  oldTag: [String],
  totalCommandsUsed: { type: Number, default: 0},
  commandsUsed: [String]
}))

exports.Command = mongoose.model('Command', new Schema({
  name: { type: String, lowerCase: true, required: true },
  count: { type: Number, default: 0 },
  servers: [{
    _id: false,
    serverID: String,
    count: Number
  }],
  users: [{
    _id: false,
    userID: String,
    count: Number
  }]
}))

exports.Info = mongoose.model('Information', new Schema({
  about: { type: String, required: true},
  values: Schema.Types.Mixed
}))

;