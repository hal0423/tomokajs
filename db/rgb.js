'use strict'

const { RoleAliases } = require('./models.js')
const db = require('../connectDB')
db.connect().then(v => {
  const a = new RoleAliases({
    name: 'rgb',
    serverID: '418811018244784129',
    roles: [],
    sort: 'color'
  })
  
  a.save()
  .then(v => {
    console.log('saved')
    db.disconnect()
  })
  .catch(console.log)
})