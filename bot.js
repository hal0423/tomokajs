'use strict'

const { Client, Collection } = require('discord.js')
const fs       = require('fs').promises
const debug    = require('debug')
const Stickers = require('./utils/stickerStructure.js')

const { authorID, config, testerPrefix } = require('./config/bot.js')
const { Server }            = require('./db/models.js')
const { getPrefix }         = require('./config/bot.js')
const { sortRolesByColors } = require('./utils/sortColors.js')

const log    = debug('bot:client')
const dbLog  = debug('db')
const errLog = debug('err:bot:client')
const client = new Client(config)

const cache = new Map()
const stuff = new Map()

client.commands         = new Collection()
client.commandAliases   = new Collection()
client.commandCountDown = new Collection()

client.servers = new Collection()

client.stickers = new Stickers()
client.cache = cache
client.stuff = stuff

client.authorID = authorID
client.tester   = new Set([authorID])

client.testerPrefix = testerPrefix

client.useDb = !!process.env.DB_ADDRESS

client.apiKeys = {
	youtube: process.env.YOUTUBE_API
}

client.loadCommand = async function() {
	this.commands.clear()
	this.commandAliases.clear()
	this.commandCountDown.clear()

	const files = await fs.readdir('./commands', {
		withFileTypes: true
	})

	log(`Loading bot commands hardly....`)

	files
		.filter(x => !x.isDirectory() && !x.name.startsWith('.'))
		.map(x => x.name)
    .forEach(x => {
			const command = requireNew(`./commands/${x}`)
			if (command.help.status === 'development') {
				return
			}

			const name = command.help.name.toLowerCase()
		
			this.commands.set(name, command)
			this.commandCountDown.set(name, new Set())

			for (const z of command.help.aliases) {
				if (!this.commandAliases.has(z)) {
					this.commandAliases.set(z, name)
				}
			}

			log(`Loaded ${name} command!`)
		})

	log(`Loaded all ${client.commands.size} commands`)

}

client.loadSticker = function() {
	this.stickers.clear()

	const PATH = `${__dirname}/img/stickers`

	fs.readdir(PATH)
	.then(stickers => {
		
		stickers.forEach(async sticker => {
			this.stickers.add(sticker.toLowerCase())

			log('Loading sticker %s', sticker)
			
			let files

			try {
				files = await fs.readdir(`${PATH}/${sticker}`)
			} catch(err) {
				errLog(err)
			}

			for (const file of files) {
				let stick
				try {
					stick = await fs.readFile(`${PATH}/${sticker}/${file}`)
				} catch(err) {
					errLog(err)
				}

				this.stickers.addValue(
					file.split('.')[0].toLowerCase().replace(/\_/g, ' '),
					sticker.toLowerCase(),
					stick
				)

			}
		})

	})
	.catch(errLog)
}

client.loadPrefix = function() {
	this.prefix = getPrefix(this.user.username)
	this.loadCustomPrefix()
	
	log('Prefix is set to %s', client.prefix)
}

client.loadCustomPrefix = async function() {
	if (!this.useDb) {
		errLog("Need a Database to load Custom Prefixes")
		return
	}

	const servers = await Server.find({
		customPrefix: { $exists: true }
	})

	if (!servers.length) {
		dbLog('Not found any custom prefix')
		return
	}

	dbLog(`Found ${servers.length} custom prefixes`)

	for (const { id, customPrefix } of servers) {
		this.servers.get(id).set('customPrefix', customPrefix)
	}
}

client.loadRoleAliases = async function() {
	if (!this.useDb) {
		errLog("Need a Database to load Role Aliases")
		return
	}

  const servers = await Server.find({
    roleAliases: { $exists: true }
  }).then(v => v.filter(x => x.roleAliases.length))

  for (const { roleAliases, id, name } of servers) {
		const server = this.servers.get(id)
		server.set('roleAliases', new Collection())

    dbLog(`Found ${roleAliases.length} role aliases on server ${name}`)
    for (const role of roleAliases) {
      let sortedRoles
    
      switch (role.sort) {
        case 'color':
          sortedRoles = sortRolesByColors(role.roles)
        break
          
        case 'default':
          sortedRoles = role.roles
        break
      }
			
			server
				.get('roleAliases')
        .set(role.name, new Set(sortedRoles.map(v => v.id)))
    }
  }
}

client.shutdown = function() {
	const leave = require('./commands/leave.js').main
	errLog(`Recevied end signal`)

	if (this.voiceConnections) {
		for (const connection of this.voiceConnections.values()) {
			log(`Disconnecting from server ${connection.channel.guild.name}...`)
			leave(connection)
		}
	}

	process.exit()
}

function requireNew(path) {
	delete require.cache[require.resolve(path)]
	return require(path)
}

exports.client = client
exports.cache = cache
exports.stuff = stuff

exports.start = token => {
	client.loadCommand()
	//client.loadSticker()
	//the sticker function isn't completed yet

	//listenning to events
	fs.readdir('./events')
	.then(v => (
		log(`Loading ${v.length} events...`), 

		v.forEach(x =>
				client.on(x.split('.')[0], (arg1 = client, arg2) => 
					require(`./events/${x}`)(arg1, arg2))
			),
		v
		)
	)
	.then(v => log(`Loaded all ${v.length} events`))
	.catch(errLog)

	log(`Login with token: ${token}`)

	process.once('SIGINT', client.shutdown)

	setInterval(() => cache.clear(), 360 * 60000)

	if (process.env.YOUTUBE_API) {
		stuff.set('youtube', process.env.YOUTUBE_API)
	}

	fs.readdir('img')
	.then(v => {
		if (v.includes('RGB Evidence')) {
			stuff.set('rgbEvidence', true)
		}
	})

	return client.login(token)
}

;