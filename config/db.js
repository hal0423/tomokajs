/*
exports.options = {
  useNewUrlParser: true,
  autoIndex: false,
  keepAlive: true,
//  dbName: 'tomoka',
//  user: 'tmokenc',
//  pass: 'Bachanglangtu',
  autoReconnect: true,
  promiseLibrary: Promise,
  family: 4
}

exports.getURL = u => {
  const users = require('./dbUser.json')
  if (!u || !users[u]) {
    return null
  }

  const {username: user, password: pass} = users[u]
  return `mongodb://${user}:${pass}@127.0.0.1:50000/tomoka?authSource=admin`
}

*/

exports.options = user => {
  let options = {
    useNewUrlParser: true,
    autoIndex: false,
    keepAlive: true,
    dbName: 'tomoka',
    autoReconnect: true,
    promiseLibrary: Promise,
    family: 4
  }
  const users = require('./dbUser.json')

  if(user && users[user]) {
    let u = users[user]

    options.user = u.username
    options.pass = u.password
    options.auth = u.auth
  }

  return options
}