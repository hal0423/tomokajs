'use strict'

const configs = require('./botConfig.json')

exports.config = {
  apiRequestMethod: 'sequential',
  shardId: 0,
  shardCount: 0,
  messageCacheMaxSize: 200,
  messageCacheLifetime: 60 * 60,
  messageSweepInterval: 60 * 45,
  fetchAllMembers: false,
  disableEveryone: true,
  sync: false
}

exports.testerPrefix = configs.testerPrefix
exports.getPrefix = name => {
  const { prefixes } = configs

  const prefix = prefixes[name.toLowerCase()] || '>'
  return prefix
}

exports.getToken = () => process.env.DISCORD_TOKEN

exports.author = configs.author
exports.authorID = configs.author.id