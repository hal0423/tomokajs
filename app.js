#!/usr/bin/env node
'use strict'

if (Number(process.version.slice(1).split(".")[0]) < 10) {
    throw new Error("Node 10.0.0 or higher is required. Update Node on your system.");
}

const debug = require('debug')

const logBot = debug('bot')
const logErrBot = debug('err:bot')
const bot = require('./bot.js')
const botConfig = require('./config/bot.js')

const dbLog = debug('db')
const dbErrLog = debug('err:db')
const db = require('./connectDB.js')

if (bot.client.useDb) {
    dbLog('Connecting to database...')
    db.connect(process.env.DB_ADDRESS)
        .then(() => dbLog('Connected to the database'))
        .catch(dbErrLog)
}

const TOKEN = botConfig.getToken()

logBot('Starting bot...')
bot.start(TOKEN)
.catch(logErrBot)