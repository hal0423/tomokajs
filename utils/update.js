'use strict'

const debug = require('debug')
const { Server } = require('../db/models.js')

const log = debug('db:update')


async function update(client) {

  if (!client.useDb) {
    throw 'Where is the DATABASE>???'
  }

  for (const server of client.guilds.array() ) {
    if (!server.available) continue
    
    let dbServer = await Server.findOne({id: server.id})
    
    const serverInfo = {
      roles: server.roles.map(v => ({
         roleID: v.id,
         name: v.name,
         mentionable: v.mentionable
       })),
       totalMember: server.memberCount,
       members: server.members.map(v => ({
         userID: v.id,
         roleIDs: v.roles.map(x => x.id)
       }))
    }
    
    if (!dbServer) {
    
        dbServer = new Server(Object.assign({
          name: server.name,
          id: server.id,
          ownerID: server.ownerID,
          announce: {
            userJoin: {
              status: false,
              channelID: server.systemChannelID
            },
            userLeave: {
              status: false,
              channelID: server.systemChannelID
            },
            userComeback: {
              status: false,
              channelID: server.systemChannelID
            }
          },
          logging: {
            status: false,
            channelID: server.systemChannelID
          }
        }, serverInfo))
        
    } else {
      Object.assign(dbServer, serverInfo)
    }


    dbServer.save()
    .then(v => log(`${v.name} has been saved to the databased`))
    .catch(log)
  }

 
}

module.exports = update