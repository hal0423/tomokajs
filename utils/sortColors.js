'use strict'

const hexToRgb = hex => {
  const toNumber = x => parseInt(x, 16)
  
  return hex.match(/.{1,2}/g).map(toNumber)
}

const colorParse = color => {
  if (typeof color === 'number') {
    return hexToRgb(color.toString(16))
  }
  if (color.startsWith('#')) {
    return hexToRgb(color.slice(1))
  }
  if (+color !== NaN) {
    return hexToRgb((+color).toString(16))
  }

}

const getHue = color => {
  const [r, g, b] = colorParse(color).map(v => v / 255)
  const min = Math.min(r, b, g)
  const max = Math.max(r, b, g)

  let hue = (min === max
            ? 0
            : max === r
                ? (g - b) / (max - min)
                : max === g
                  ? 2 + (b - r) / (max - min)
                  : 4 + (r - g) / (max - min)
            ) * 60
  ;

  if (hue < 0) {
    hue += 360
  }
  
  return Math.round(hue)
}

const getLuminosity = color => {
  const [r, g, b] = colorParse(color).map(v => v / 255)
  return Math.sqrt(0.2126 * r + 0.7152 * g + 0.0722 * b)
}
/*
const getLuminosity2 = color => {
  const [r, g, b] = colorParse(color).map(v => v / 255)
  return Math.sqrt(0.299 * r + 0.587 * g + 0.114 * b)
}

const getLuminosity3 = color => {
  const [r, g, b] = colorParse(color).map(v => v / 255)
  return Math.sqrt(0.2126 * r + 0.7152 * g + 0.0722 * b)
}
*/
const sortColors = colors => 
  colors.sort((a, b) => getHue(b) - getHue(a))
;

const sortRolesByColors = roles => 
  //roles.sort((a, b) => getHue(a.color) - getHue(b.color))
  //roles.sort((a, b) => getLuminosity(a.color) - getLuminosity(b.color))
  roles.sort((a, b) => getLuminosity(a.color) - getLuminosity(b.color))
;

exports.getHue = getHue
exports.sortColors = sortColors
exports.sortRolesByColors = sortRolesByColors
;






/*
const getHSL = color => {
  const [r, g, b] = colorParse(color).map(v => v / 255)

  const min = Math.min(r, b, g)
  const max = Math.max(r, b, g)
  
  const hasSaturation = min !== max

  const luminace = Math.round(((min + max) / 2) * 100)

  let saturation = 0
  if (hasSaturation) {
    saturation = luminace < 50
                  ? (max - min) / (max + min)
                  : (max - min) / (2 - max - min)
    ;

    saturation = Math.round(saturation * 100)
  }

  let hue = (hasSaturation
              ? max === r
                ? (g - b) / (max - min)
                : max === g
                  ? 2 + (b - r) / (max - min)
                  : 4 + (r - g) / (max - min)
              : 0
            ) * 60
  ;
  
  if (hue < 0) {
    hue += 360
  }

  hue = Math.round(hue)
  
  return {
    hue,
    luminace,
    saturation
  }
}
*/