'use strict'

function Node() {
  this.data = new Map()
  this.end = false
  this.values = false
}

function Tries() {
  this.root = new Node()

  this.add = function (input, node = this.root) {
    if (!input) {
      node.end = true
      return
    }
    if (!node.data.has(input[0])) node.data.set(input[0], new Node())
    return this.add(input.substr(1), node.data.get(input[0]))
  }
  this.addValue = function (input, name, value, node = this.root) {
    if (!input) {
      if (!node.values) {
        node.values = { [name]: [value] }
      } else if (!node.values[name]) node.values[name] = [value]
      else node.values[name].push(value)
      return
    }
    if (!node.data.has(input[0])) node.data.set(input[0], new Node())
    return this.addValue(input.substr(1), name, value, node.data.get(input[0]))
  }

  this.has = function (word) {
    return this._isThere(word)
  }
  this.hasValues = function (word) {
    return Boolean(this._isThere(word, true))
  }
  
  this._isThere = function (word, values = false) {
    let node = this.root
    if (!word) return
    while (word.length > 1) {
      if (!node.data.has(word[0])) return false
      node = node.data.get(word[0])
      word = word.substr(1)
    }
    if (!node.data.has(word)) return false
    
    const data = node.data.get(word)
    
    return values ? data.values : data.end
  }
  
  this.getWords = function () {
    const { words } = this.getAll()
    return words
  }
  this.getValue = function (value, autocorrect = false) {
    let result
    if (autocorrect) {
      result = {}
      const a = this.getAllValues()
      const b = []
      Object.keys(a).forEach(x => {
        const c = Object.keys(a[x]).filter(v => v.startsWith(value))
        if (c.length) b.push({ name: x, key: c})
      })
      console.log(b)
      b.length 
      ? b.forEach(v => v.key.forEach((x, j) => {
        // console.log(v.name)
        result[x]
        ? result[x].push(a[v.name][x][0])
        : result[x] = [a[v.name][x][0]]
      }))
      : result = false
      console.log(result)
    } else result = this._isThere(value, true)
    return result
  }
  this.getAllValues = function () {
    const { val } = this.getAll()

    return val
  }
  this.getValuesByWord = function (word) {
    if (!this.has(word)) throw new Error('Doesn\'t have word') 
    const { val } = this.getAll()
    const result = Object.keys(val[word]).reduce((a, z) => a.concat(val[word][z]), [])
    // console.log(result)
    return result
  }
  this.getValuesName = function () {

  }

  this.getAll = function (word) {
    const words = []
    const val = {}

    function search(node, string) {
      if (!node.data.size && !node.end) return
      
      for (let letter of node.data.keys()) {
        search(node.data.get(letter), string.concat(letter));
      }
      if (node.end) words.push(string)
      if (node.values) Object.keys(node.values).forEach(v => {
        val[v]
        ? val[v][string] = node.values[v] 
        : val[v] = {[string]: node.values[v]}
      })
    }
    search(this.root, '')
    // console.log(val)

    return { words, val }
  }

  this.clear = function() {
    this.root = new Node()
    return this
  }
}

module.exports = Tries

/*
const myTrie = new Tries()
myTrie.add('ball'); 
myTrie.add('bat'); 
myTrie.add('doll'); 
myTrie.add('dork'); 
myTrie.add('do'); 
myTrie.add('dorm')
myTrie.add('send')
myTrie.add('sense')
myTrie.addValue('a', 'aa')
myTrie.addValue('d', 'done')
myTrie.addValue('loli', 'menhera', 'fat')
myTrie.addValue('loli', 'menhera', 'morning')
myTrie.addValue('lolita', 'Nico', 'nii')
myTrie.addValue('lolita', 'Nico', 'nii')
myTrie.addValue('lolita', 'Nico', 'nii')
*/


// console.log(myTrie.hasWord('doll'))
// console.log(myTrie.hasWord('dor'))
// console.log(myTrie.hasWord('dorf'))
// console.log('loli values is', myTrie.hasValues('loli'))
// console.log('pop values is', myTrie.hasValues('pop'))
// console.log('lolita values is', myTrie.hasValues('lolita'))

// // console.log(myTrie.getAll())
// console.log(myTrie.getAllValues())
// console.log(myTrie.getValue('loli'))