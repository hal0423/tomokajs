'use strict'

function Help(name = '') {
  if (name.split(' ').length > 1) {
    throw new Error('name can\'t be uses with space')
  }
  
  const defaultSetting = {
    name,
    aliases: [],
    dm: false,
    description: '',
    usage: '%p' + name,
    countDown: 4000,
    permission: ['SEND_MESSAGES'],
    userPermission: [],
    selfHandle: false,
    usingDB: false,
    status: 'development'
  }

  return Object.assign(this, defaultSetting)
}

Help.prototype = {
  setName(name) {
    this.name = name
    return this
  },
  
  setDm(allow = true) {
    this.dm = allow
    return this
  },

  setAliases(aliases) {
    this.aliases = aliases
    return this
  },
  
  setDescription(desc) {
    this.description = desc
    return this
  },

  setUsage(usage) {
    this.usage = usage
    return this
  },

  setCountDown(cd) {
    this.countDown = cd
    return this
  },

  setPermission(permission) {
    this.permission = permission
    return this
  },
  
  setUserPermission(permission) {
    this.userPermission = permission
    return this
  },

  setSelfHandle(canHandle) {
    this.selfHandle = canHandle
    return this
  },

  setStatus(status) {
    this.status = status
    return this
  },

  //Another form
  
  allowDM(yep = true) {
    return this.setDm(yep)
  },

  canHandle(yep = true) {
    return this.setSelfHandle(yep)
  },

  useDB(yep = true) {
    this.usingDB = yep
    return this
  },

  forAuthor() {
    return this.setUserPermission('author')
  },

  stable() {
    return this.setStatus('stable')
  },

  beta() {
    return this.setStatus('beta')
  }
}

Help.init = (name = '') => new Help(name)

module.exports = {
  Help
}