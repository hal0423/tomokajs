'use strict'

// const { fetch } = require('./myUtils')

const getLineSticker = async url => {
	const body = await fetch(url)

	if (body.includes('<div class="MdMN05Error')) {
		return Promise.reject('This sticker is not available')
	}

	const TYPE = ['sticker','animation','sound','popup']
	const SOURCE = ['ANDROID/sticker@2x.png','IOS/sticker_animation@2x.png','IOS/main_sound.m4a','IOS/sticker_popup.png']

	let [title, type, ids] = body.split('\n')
		.filter(v => (v = v.trim(), v.startsWith('type: ') || v.startsWith('ids: ') || v.startsWith('<title>')))
		.map(v => v.trim())

	ids = ids.match(/\d+/g)
	type = type.match(/\w+/g)[1] || 'sticker'
	title = title.match(/[\w -.]+/g)[1].trim()

  const source = SOURCE[ TYPE.indexOf(type) ]
	const list = ids.map(id => ([
		id, `https://stickershop.line-scdn.net/stickershop/v1/sticker/${id}/${source}`
	]) )
	
	return Promise.resolve({ title, type, list })
}

// for (const a of Object.values(menhera)) {
// 	for (const b of Object.values(a)) {
// 		getLineSticker(b)
// 		.then(async v => {
// 			const dirName = v.title + (v.type === 'sticker' ? '' : ' - ' + v.type)
// 			try {
// 				await fs.mkdir(dirName)
// 			} catch (err) {
// 				console.log(err.path + ' exited')
// 			}

// 			for (const [name, url] of v.list) {
// 				console.log(url)
// 				const img = await fetchImage(url)
// 				const buf = Buffer.from(img.data, 'binary')
				
// 				fs.writeFile(dirName + '/' + name + '.png', buf)
// 				console.log(name)
// 			}
// 			console.log('done ' + v.title)
// 		})info => {}
// 		.cinfo => {}h(console.log)
// 	}
// }

class LineSticker {
	constructor(...arg) {
		
		this.TYPE = ['sticker', 'animation', 'sound', 'popup']
		this.SOURCE = [
			'ANDROID/sticker.png', 
			'IOS/sticker_animation@2x.png', 
			'IOS/main_sound.m4a', 
			'IOS/sticker_popup.png'
		]

		const Events = require('events')

		this.event = new Events()

		this.isProcessing = false
		this.saved = null

		if (arg[0]) {
			this.newSticker = arg[0]
		}
		
	}

	set newSticker(url) {
		this.url = url
		this.hasSticker = true
		this.fetchSticker().catch(console.log)
	}

	async fetchSticker() {
		if (this.isProcessing) {
			throw 'ARGGHHHHHHHHH!!!!!!!!'
		}

		this.isProcessing = true
		const body = await this._fetch(this.url)

		if (body.includes('<div class="MdMN05Error')) {
			this.isProcessing = false
			this.event.emit('error', 'This sticker is not available for me right now')
			throw 'This sticker is not available for me'
		}

		let [author, title, desc, type, ids] = body.split('\n')
			.filter(v => (v = v.trim(), v.startsWith('type: ') || 
																	v.startsWith('<a href="/stickershop/author') ||
																	v.startsWith('<p class="mdCMN08Desc">') ||
																	v.startsWith('ids: ') || 
																	v.startsWith('<h3 class="mdCMN08Ttl')))
			.map(v => v.trim())
		;

		function getContent(e) {
			return e.split('>')[1].split('<')[0]
		}

		const authorUrl = 'https://store.line.me' + 
			author.split('href="')[1].split('"')[0]
		
		author = getContent(author)
		desc   = getContent(desc)
		ids    = ids.match(/\d+/g)
		type   = type.match(/\w+/g)[1] || 'sticker'
		title  = getContent(title)

		const source = this.SOURCE[ this.TYPE.indexOf(type) ]
		const list = ids.map(id => ([
			id, `https://stickershop.line-scdn.net/stickershop/v1/sticker/${id}/${source}`
		]))

		this.title = title
		this.author = author
		this.authorUrl = authorUrl
		this.description = desc
		this.type = type
		this.stickerList = list

		this.isProcessing = false
		this.hasSticker = true
		this.event.emit('fetched', this)

		return this
	}

	getInfo() {
		if (!this.hasSticker || this.isProcessing) {
			return null
		}

		return {
			url: this.url,
			title: this.title,
			author: this.author,
			authorUrl: this.authorUrl,
			description: this.description,
			type: this.type,
			stickerList: this.stickerList
		}
	}

	_getFile(url) {
		return this._fetch(url, true).then(v => Buffer.from(v, 'binary'))
	}

	async save({
		saveTo = process.env.PWD,
		toOwnFolder = true,
		asZip = false,
		toBuffer = false
	} = {}) {
		if (this.isProcessing) {
			throw 'The sticker set is being processed, please wait a few seconds'
		}

		if (!this.hasSticker) {
			throw 'There is no sticker yet'
		}

		const fs = require('fs').promises
		const path = require('path')
		const JSZip = require('jszip')

		saveTo = path.resolve(saveTo)
		const { title, type, stickerList } = this.getInfo()

	
		let buffers = await Promise.all(stickerList.reduce((v, x) => v.concat([this._getFile(x[1])]), []))
		for (let i = 0; i < stickerList.length; i++) {
			stickerList[i][1] = buffers[i]
		}

		buffers = null

		const folderName = title + (type === 'sticker' ? '' : " - " + type)
		const dirName = path.resolve(saveTo, (toOwnFolder ? folderName : '.'))

		let out = dirName

		if (asZip) {
			out += '.zip'
			const zip = new JSZip()

			for (const [id, src] of stickerList) {
				zip.file((toOwnFolder ? folderName + '/' : '') + id + '.png', src)
			}

			const zipData = await zip.generateAsync({ type: 'nodebuffer' })
			if (toBuffer) {
				this.event.emit('saved', {})
				return zipData
			}

			await fs.writeFile(path.resolve(saveTo) + '/' + folderName + '.zip', zipData)

		} else {
			
			if (toOwnFolder) {
				const dirList = await fs.readdir(saveTo, { withFileTypes: true })
					.then(e => e.reduce((v, x) => x.isDirectory() ? v.concat([x.name]) : v, []))
				;
	
				if (!dirList.includes(folderName)) {
					await fs.mkdir(dirName, { recursive: true })
				}
	
			}
	
			for (const [name, data] of stickerList) {
				await fs.writeFile(dirName + '/' + name + '.png', data)
			}
		}

		this.event.emit('saved', {out})

		return this
	}

	_fetch(url, binary = false) {
		return new Promise((resolve, reject) => {
			const lib = require(url.match(/\w+/).toString().toLowerCase())

			const req = lib.get(url, res => {
				if (res.statusCode < 200 || res.statusCode > 299) {
					throw new Error('Failed to load page, status code: ' + res.statusCode)
				}

				if (binary) {
					res.setEncoding('binary')
				}

				let html = ''

				res.on('data', chunk => html += chunk)
				res.on('end', () => resolve(html))
			})

			req.on('error', reject)
		})

	}
}

module.exports = {
	getLineSticker,
	LineSticker
}
/*
// */
// const test = "https://store.line.me/stickershop/product/3474045"
// const a = new LineSticker(test)


// a.event.once('fetched', () => {
// 	a.save({saveTo: 'tmp', asZip: true})
// })

// a.event.once('saved', console.log)
// // setTimeout(function() {
// // 	console.log(a.getInfo())
// // }, 5000)