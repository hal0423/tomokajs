'use strict'
// data.length && data.constructor === Array
const arithmetic = data => 
  data.reduce((v, x) => v + x, 0) / data.length    
;

const geometric = data => 
  data.reduce((v, x) => v * x, 1) ** (1 / data.length)
;

const harmonic = data => 
  data.length / data.reduce((v, x) => v + (1 / x), 0)
;

const quadratic = data =>
  Math.sqrt(data.reduce((v, x) => v + Math.pow(x), 0))
;

class Statistics {
  constructor(...data) {
    this.newData = data[0]

    this.fixed = data[1] || 4

    this.a = this.arithmetic
    this.g = this.geometric
    this.h = this.harmonic
    this.k = this.quadratic

    this.sd = this.standardDeviation

  }


  arithmetic() {
    if (!this.computed.arithmetic) {
      this.computed.arithmetic = +(this.sumData / this.n).toFixed(this.fixed)
    }

    return this.computed.arithmetic
  }

  geometric() {
    if (!this.computed.geometric) {
      const exp = 1 / this.n
      this.computed.geometric = +(this.data.reduce((v, x) => v * x, 1) ** exp).toFixed(this.fixed)
    }

    return this.computed.geometric
  }

  harmonic() {
    let { sumDataDiv } = this.computed

    if (!sumDataDiv) {
      const dataDiv = [...this.data].map(v => this._fixPointer(1 / v)) 
      sumDataDiv = this._sum(dataDiv)
      
      this.computed.dataDiv = dataDiv
      this.computed.sumDataDiv = sumDataDiv
    }

    if (!this.computed.harmonic) {
      this.computed.harmonic = +(this.n / sumDataDiv).toFixed(this.fixed)
    }

    return this.computed.harmonic
  }

  quadratic() {
    if (!this.computed.sumDataPow) {
      const dataPow = [...this.data].map(v => this._fixPointer(v ** 2))

      this.computed.dataPow = dataPow
      this.computed.sumDataPow = this._sum(dataPow)
    }

    const { sumDataPow } = this.computed

    if (!this.computed.quadratic) {
      this.computed.quadratic = +Math.sqrt(sumDataPow).toFixed(this.fixed)
    }

    return this.computed.quadratic
  }

  absoluteDeviation() {
    if (!this.computed.absoluteDeviation) {
      this.computed.absoluteDeviation = +(this.data.reduce((v, x) => 
        v + Math.abs(x - this.arithmetic()), 0) / this.n
      ).toFixed(this.fixed)
    }

    return this.computed.absoluteDeviation
  }

  squareDeviation(s = false) {
    const name = s ? 'squareDeviation' : 'populationSquareDeviation'
    if (!this.computed[name]) {
      this.computed[name] = +(this.data.reduce((v, x) => 
        v + ((x - this.arithmetic()) ** 2), 0) / (this.n - (s ? 1 : 0))
      ).toFixed(this.fixed)
    }

    return this.computed[name]
  }

  standardDeviation(s = false) {
    const name = s ? 'standardDeviation' : 'populationStandardDeviation'
    if (!this.computed[name]) {
      this.computed[name] = Math.sqrt(this.squareDeviation(s))
        .toFixed(this.fixed)
    }

    return this.computed[name]
  }

  set newData(data) {
    if (data.constructor !== Array && !data.length)
      throw new Error('This has to be an array of data')
    ;

    this.data = data
    this.sumData = this._sum(this.data)

    this.n = data.length

    this.computed = {}

  }

  _sum(data) {
    return data.reduce((v, x) => this._fixPointer(v + x), 0)
  }

  _fixPointer(x) {
    return +x.toPrecision(15)
  }
}

module.exports = {
  arithmetic, 
  geometric, 
  harmonic, 
  quadratic,
  Statistics
} 

// const a = new Statistics([50, 500, 1000, 3000, 5000, 10000, 25000])
// console.log(`Arithmetic mean of [${a.data}]`, a.arithmetic())
// console.log(`Geometric mean of [${a.data}]`, a.geometric())
// console.log(`Harmonic mean of [${a.data}]`, a.harmonic())
// console.log(`Quadratic mean of [${a.data}]`,a.quadratic())
// console.log(`Absolute deviation of [${a.data}]`, a.absoluteDeviation())
// console.log(`Square deviation of [${a.data}]`, a.squareDeviation())
// console.log(`Standard deviation of [${a.data}]`, a.standardDeviation())

// const a = new Statistics([10, 2, 38, 23, 38, 23, 21])
// console.log(`Arithmetic mean of [${a.data}]`, a.arithmetic())
// console.log(`Geometric mean of [${a.data}]`, a.geometric())
// console.log(`Harmonic mean of [${a.data}]`, a.harmonic())
// console.log(`Quadratic mean of [${a.data}]`,a.quadratic())
// console.log(`Absolute deviation of [${a.data}]`, a.absoluteDeviation())
// console.log(`σ^2 of [${a.data}]`, a.squareDeviation())
// console.log(`σ of [${a.data}]`, a.standardDeviation())
// console.log(`s^2 of [${a.data}]`, a.squareDeviation(true))
// console.log(`s of [${a.data}]`, a.standardDeviation(true))

// const a = new Statistics([1.2, 6, 2.7, 6])
// console.log(`Arithmetic mean of [${a.data}]`, a.arithmetic())
// console.log(`Geometric mean of [${a.data}]`, a.geometric())
// console.log(`Harmonic mean of [${a.data}]`, a.harmonic())
// console.log(`Quadratic mean of [${a.data}]`,a.quadratic())