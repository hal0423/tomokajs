'use strict'

const debug = require('debug')

const { Info, Command } = require('../db/models.js')

const log = debug('bot:event:message')
const errLog = debug('err:bot:event:message')
const dbErrLog = debug('err:db:message')

const command = {
  
  isAuthor() {
    return this.author.id === this.client.authorID
  },

  isTester() {
    return this.client.tester.has(this.author.id)
  },

  usingAsTester() {
    return this._checkPrefix(this.client.testerPrefix) 
        && this.isTester()
  },

  matchPrefix({
    guild ,
    client: { prefix, servers, testerPrefix } 
  } = this) {
    const server  = guild && servers.get(guild.id)
    const cPrefix = server && server.get('customPrefix')

    const realPrefix = cPrefix || prefix

    const checkTester = this.isTester() && this._checkPrefix(testerPrefix) && testerPrefix

    return this._checkPrefix(realPrefix)
            ? realPrefix
            : checkTester
  },

  _checkPrefix(prefix) {
    return this.content.toLowerCase().startsWith(prefix)
  },


  _hasPermission(permission) {
    if (!permission.length) return true
  
    const per = this.channel.permissionsFor(this.client.user)
    return per.has(permission)
  },

  _userHasPermission(permission, user = this.member) {
    if (!permission || !permission.length || this.isAuthor()) {
      return true
    }

    if (typeof permission === 'string') {
      permission = permission.toLowerCase()

      if (permission === 'author') {
        return this.isAuthor()
      }

      if (permission === 'tester') {
        return this.isTester()
      }
    
    }

    return user.hasPermission(permission)
  },

  _getRoleAliases(roles, content) {
    const reg = /(?:^|\s)[@!](\w+)(?!\w)/gi
    let temp = reg.exec(content)
  
    while (temp) {
      if (roles.has(temp[1])) break
  
      temp = reg.exec(content)
    }
  
    return temp && roles.get(temp[1])
  },
  
  processRoleAliases(roles) {
    return new Promise(resolve => {
      const role = this._getRoleAliases(roles, this.content)
  
      if (!role || !role.size) {
        return resolve()
      }
  
      log('Found a role aliases')
  
      const mess = [...role].reduce((v, x) => v + `<@&${x}>`, '')
      this.channel.send(mess, {
        split: {
          maxLength: 1980,
          char: '>',
          append: '>'
        } 
      })
      .then(resolve)
      .catch(errLog)
    })
  },

  matchWords(wordList) {
    const text  = this.content.toLowerCase().split(' ')
    const match = text.reduce((v, x) => wordList.includes(x) ? [...v, x] : v , [])

    return [...new Set(match)].map(v => v.toUpperCase())
  },

  _hasSticker() {

  },
  
  _getCommand(command, { commands, commandAliases } = this.client) {
  
    if (commandAliases.has(command)) {
      command = commandAliases.get(command)
    }
    
    if (!commands.has(command)) {
      return 
    }
  
    return commands.get(command)
  },
  
  processCommand(prefix, command) {
    return new Promise((resolve, reject) => {
      command = command.toLowerCase()
      const cmd = this._getCommand(command)
  
      if (!cmd) {
        errLog(`${command} isn't a command`)
        return resolve()
      }
      
      if (!cmd.help.dm && ['dm', 'group'].includes(this.channel.type)) {
        errLog(`The ${command} cannot be used on DM channel`)
        return resolve()
      }

      if (cmd.help.usingDB && !this.client.useDb) {
        errLog(`${command} is a database to be processed`)
        return resolve()
      }

      if (this.channel.type !== 'dm') {
        
        if (!this._userHasPermission(cmd.help.userPermission)) {
          throw `I'm sorry but you don't have enough permission for this command.\`\`\`Permission required: ${cmd.help.userPermission}\`\`\``
        }
    
        if (!this._hasPermission(cmd.help.permission)) {
          throw `Sorry but I don't have enough permission for this`
        }
      }
      
      // const isTester = prefix === this.client.testerPrefix
      const isTester = this.client.tester.has(this.author.id)
      if (!isTester && cmd.help.status === 'beta') {
        throw `I'm sorry but this command isn't stable yet, it's for beta tester only.`
      }
      
      const cd = this._countDown(cmd.help.name, cmd.help.countDown)
      if (!cd) {
        throw `${this.author}, this command has ${cmd.help.countDown / 1000}s count down, please be patient`
      }
  
  
  
      const content = this.content.slice(prefix.length + command.length).trim()
      const serverInfo = this.guild && this.client.servers.get(this.guild.id)
      
      cmd.run(this, { content, serverInfo })
      .then(async mess => {
        if (!cmd.help.selfHandle) {
          try {
            mess = await this.channel.send(mess)
          } catch(err) {
            return reject(err)
          }
        }

        const cmdName = cmd.help.name
        
        if (this.useDb) {
          this.updateDB(cmdName)
        }

        resolve(mess)
      })
      .catch(reject)
  
    })
  },

  
  _countDown(command, countDown, userID = this.author.id) {
    if (this.isAuthor()) {
      return true
    }

    const cmd = this.client.commandCountDown.get(command.toLowerCase())
    
    if (cmd.has(userID)) {
      return false
    }
  
    cmd.add(userID)
  
    return this.client.setTimeout(function() {
      cmd.delete(userID)
    }, countDown)
  },

  updateDB(command, mess = this) {
    const check = (type, value) => {
      if (type === 'user') return value.userID === mess.author.id
      if (type === 'server') return value.serverID === mess.guild.id
    }
  
    Info.findOne({about: 'command'})
      .then(info => {
        info.values.totalUsed++
  
        const userIndex = info.values.usedBy.findIndex(e => check('user', e))
        const serverIndex = info.values.usedOn.findIndex(e => check('server', e))
  
        if (userIndex === -1) info.values.usedBy.push({userID: mess.author.id, count: 1})
        else info.values.usedBy[userIndex].count++
  //      info.markModified(`values.usedBy`)
  
        if (serverIndex === -1) info.values.usedOn.push({serverID: mess.guild.id, count: 1})
        else info.values.usedOn[serverIndex].count++
  //      info.markModified(`values.usedOn`)
  
  
        info.markModified('values')
        info.save()
        .catch(dbErrLog)
  
      }).catch(dbErrLog)
  
    Command.findOne({name: command})
      .then(cmd => {
        if (!cmd) {
          cmd = new Command({
            name: command,
            count: 1,
            servers: [{
              serverID: mess.guild.id,
              count: 1
            }],
            users: [{
              userID: mess.author.id,
              count: 1
            }]
          })

  
        } else {
          cmd.count++
          const serverIndex = cmd.servers.findIndex(e => check('server', e))
          const userIndex = cmd.users.findIndex(e => check('user', e))
  
          if (serverIndex === -1) cmd.servers.push({serverID: mess.guild.id, count: 1})
          else cmd.servers[serverIndex].count++
  
          if (userIndex === -1) cmd.users.push({userID: mess.author.id, count: 1})
          else cmd.users[userIndex].count++
  
        }
  
        cmd.save()
        .catch(dbErrLog)
  
      }).catch(dbErrLog)
  
  }

}

module.exports = l => Object.assign(l, command)