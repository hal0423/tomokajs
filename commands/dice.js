'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')

const log = debug('bot:command:dice')
const errLog = debug('bot:err:command:dice')

function dice(message) {
  return new Promise(async (resolve, reject) => {
    const content = `**${message.author.username}** đổ xí ngầu và được **${Math.ceil(Math.random() * 6)} điểm**`
    
    resolve(content)    
  })
}

const help = Help.init('dice')
  .setAliases(['roll'])
  .setDescription('dice a dice')
  .setCountDown(2500)
  .stable()

exports.run = dice
exports.help = help


;
