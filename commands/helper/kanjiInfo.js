'use strict'

const request = require('request-promise')
const debug = require('debug')('err')

exports.kanjiInfo = z => request(encodeURI(`http://mazii.net/api/mazii/${z}/${z.length}/`))
    .then(a => JSON.parse(a))
    .then(a => {
      if (a.status !== 200) return false
      let c = z.split(''), b = ''

      a.results.forEach(y => c[c.indexOf(y.kanji)] = y)

      return c.filter(x => x.mean !== undefined)
    })
    .catch(debug)
