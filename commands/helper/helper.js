'use strict'

exports.capitalize = x => 
  x.toLowerCase().replace(/(?:^|\s)\S/g, a => a.toUpperCase())

