'use strict'

const { Help } = require('../utils/help.js')

const jailrgb = () => ({
  image: {
    url: "https://cdn.discordapp.com/attachments/512404979772948489/549828250118127628/RGB-in-jail1.gif"
  },
  color: 0xffffff * Math.random() << 0
})

const handler = () => Promise.resolve({ embed: jailrgb() })

const help = Help.init('jailrgb')
  .setAliases(['rgbinjail', 'rgbjail', 'j'])
  .stable()

exports.run = handler
exports.help = help
exports.main = jailrgb

;
