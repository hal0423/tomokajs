'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')
const { LineSticker } = require('../utils/lineSticker.js')
const url = require('url')
const he = require('he')

const log = debug('bot:command:lineSticker')
const errLog = debug('err:bot:command:lineSticker')

const getList = (list, length = 950) => {
  
  const result = ['']
  let current = 0

  for (const [id, n] of list) {
      const test = result[current] + n + '\n'
      
      if (test.length >= length) {
        result.push(n + '\n')
        current++
      } else {
        result[current] = test
      }
  }

  return result
}

const lineSticker = (message, { content }) => 
  new Promise(async (resolve, reject) => {
    const cont = content.match(/\S+/gm)
    const flag = {
      zip: false
    }
    if (cont.length > 1) {
      if (cont.includes('--zip')) {
        cont.splice(cont.indexOf('--zip'), 1)
        flag.zip = true
      }
    }

    const uri = url.parse(cont.toString().replace(/\<|\>/g, ''))

    if (uri.hostname !== 'store.line.me') {
      return reject(`Nope, \`${content}\` is not a valid LINE store URL`)
    }

    const a = uri.path.slice(1).split('/')

    if (a.length < 3 ||
      a[0] !== 'stickershop' &&
      a[1] !== 'product' &&
      +a[2] !== NaN) {
      
      return reject('Nope, not a LINE sticker store')
    }

    const sticker = new LineSticker(uri.href)

    sticker.event.once('error', err => {
      sticker.event.off('fetched', () => {
        log('error')
      })

      return reject(err)
    })

    sticker.event.once('fetched', async() => {
      
      sticker.event.off('error', () => {
        log('fetched')
      })

      const info = sticker.getInfo()

      const embed = {
        "title": he.decode(info.title),
        "description": he.decode(info.description) + '\n' + 'Type:  __' + info.type + '__',
        "url": info.url,
        "color": 10625791,
        "thumbnail": {
          "url": info.stickerList[info.stickerList.length * Math.random() << 0][1]
        },
        "image": {
          "url": info.stickerList[info.stickerList.length * Math.random() << 0][1]
        },
        "author": {
          "name": info.author,
          "url": info.authorUrl
        }
        
      }


      if (flag.zip) {
        const data = await sticker.save({  
          asZip: true, 
          toBuffer: true 
        })
        const size = +(data.length / 1024 / 1024).toFixed(2)
        embed.description += `\nSize: ${size} MB`
        embed.file = {
          attachment: data,
          name: info.title + '.zip'
        }

      } else {
        embed.fields = []

        for (const list of getList(info.stickerList)) {
          embed.fields.push({
            name: ''.padEnd((embed.fields.length + 1), '>'),
            value: list
          })
        }
      }      

      return resolve({embed})

    })

    
  })



const help = Help.init('linesticker')
  .setAliases(['line'])
  .setDescription('Get lineSticker')
  .setCountDown(30000)
  .stable()

exports.run = lineSticker
exports.help = help


;
