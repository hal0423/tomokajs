'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')
const { kanjiInfo } = require('./helper/kanjiInfo.js')

const log = debug('bot:command:kanji')
const errLog = debug('bot:err:command:kanji')

const wordLimit = x => {
  let a = ''
  
  for (const i of x) {
    let b = a + '- ' + i + '\n'
    if (b.length > 1024) break
    
    a = b
  }
  
  return a
}

function kanji(message, { content }) {
  return new Promise(async (resolve, reject) => {
    let kanji
    
    try {
      kanji = await kanjiInfo(content)
    } catch (err) {
      errLog(err)
      return reject(err)
    }
    
    if (!kanji || !kanji.length) {
      return reject(`Error 404: Kanji not found`)
    }
    
    const embed = {
      fields: [],
      color: 0x977df2
    }
    
    kanji.forEach(v => embed.fields.push({ 
      name: `${v.kanji} - ${v.level ? `(N${v.level}) ` : ''}${v.mean} | ${v.on.replace(/\ /g, '、')} ${v.kun ? `| ${v.kun.replace(/\ /g, '、')}` : ''}`, 
      value: wordLimit(v.detail.split('##'))
    }))
    
    log(embed)
    resolve({embed})
  })
  
}

const help = Help.init('kanji')
.setAliases(['k'])
.setDescription('[Vietnamese] tìm thông tin về hán tự')
.setUsage('%pkanji <kanji>')
.stable()

exports.run = kanji
exports.help = help


;
