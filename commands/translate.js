'use strict'

const debug    = require('debug')
const trans    = require('google-translate-api')
const { Help } = require('../utils/help.js')
const tieqViet = require('./helper/tieqViet.js')

const log    = debug('bot:command:translate')
const errLog = debug('err:bot:command:translate')

const parseConfig = text => {
  const lang = {
    from: 'auto',
    to:   'vi'
  }

  const ex = text.match(/\[(.*)?\]/)
  if (ex) {
    let v = ex[1].split('>')

    if (v[0])          lang.from = v[0]
    if (v[0] === 'vi') lang.to = 'en'
    if (v[1])          lang.to = v[1]

    text = text.replace(ex[0], '').trim()
  }

  return { text, lang }
}

const translate = (message, { content }) => 
  new Promise (async (resolve, reject) => {
    if (!content) {
      return reject({ message: 'Please enter something to translate' })
    }

    const { text, lang } = parseConfig(content)

    let translation
    try { 
      translation = await trans(text, lang) 
    } catch (err) {
      errLog(err)
      return reject({ message: `Cannot get the translation from google transalte` })
    }

    const { iso, didYouMean } = translation.from.language
    const {
      value,
      autoCorrected: textAutoCorrect,
      didYouMean: textDidYouMean
    } = translation.from.text

    const ntv = iso !== 'vi' || lang.to !== 'vi'

    const language = {
      text: ntv ? translation.text : tieqViet(text),
      from: trans.languages[iso],
      to: ntv 
        ? trans.languages[lang.to]
          ? trans.languages[lang.to]
          : lang.to
        : 'Tiếq Việt'
    }

    const embed = {
      title: `${language.from} -> ${language.to}`,
      description: '=> ' + language.text
    }

    if (didYouMean) {
      embed.fields = [{
        name: 'Did you mean',
        value: 'language: ' + trans.languages[iso],
        inline: true
      }]
    }

    if (textAutoCorrect || textDidYouMean) {
      const i = {
        name: 'Autocorrected',
        value: value.replace(/\[|\]/g, '___'),
        inline: true
      }

      embed.fields ? embed.fields.push(i) : embed.fields = [i]
    }

    log(embed)
    resolve({ embed })
        
  })

const help = Help.init('translate')
  .setAliases(['trans', 't'])
  .setDescription('from Google-sensei with love')
  .setUsage('%ptranslate [ <?from> > <?to> ] <textToTrans>')
  .setCountDown(8000)
  .stable()


exports.run = translate
exports.help = help


;