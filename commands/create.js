'use strict'

const debug = require('debug')

const log = debug('bot:command:create')
const errLog = debug('bot:err:command:create')

const create = (message, { content }) => 
new Promise(async (resolve, reject) => {
  log("LoliChronicle")

  try {
    const mess = await message.channel.send()
    resolve(mess)
  } catch (err) {
    errLog(err)
    reject({message: `Something went wrong!`})
  }    
})



const help = {
  name: 'create',
  aliases: [],
  description: 'Create something',
  usage: 'create <what>',
  countDown: 4000,
  permission: ['SEND_MESSAGES'],
  status: 'development'
}

exports.run = create
exports.help = help


;
