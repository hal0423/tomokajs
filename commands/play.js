'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')

const ytdl = require('ytdl-core')

const log = debug('bot:command:play')
const errLog = debug('err:bot:command:play')

const play = arg => {
  log("LoliChronicle")
  return 'Nothing yet!'
}

const handler = async (message, { content }) => {

}

const help = Help.init('play')
  .setAliases(['p', 'pl'])
  .setDescription('play music')
  .setUsage('')

exports.run = handler
exports.help = help
exports.main = play

;
