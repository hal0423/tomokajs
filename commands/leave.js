'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')

const log = debug('bot:command:leave')
const errLog = debug('err:bot:command:leave')

const leave = ({ channel, client }) => {
    log(`Leaving the voice channel ${channel.name}`)
    channel.leave()
    client.servers.get(channel.guild.id).delete('voice')
}

const handler = async (message) => {
  const { guild: { voiceConnection }, member: { voiceChannel } } = message

  if (!voiceConnection) {
    errLog("Nowhere to leave")
    throw "I have nowhere to leave"
  }

  if (!message.isAuthor()) {
    if (!voiceChannel) {
      throw "Huh??? Where are you??? I can't hear you"
    }

    if (voiceConnection.channel.id !== voiceChannel.id) {
      throw "I can't hear you. We aren't in the same channel"
    }
  }

  leave(voiceConnection)

  return `I'm freeeeeeeeeee!!!!`
}



const help = Help.init('leave')
  .setDescription("Leave the voice channel")
  .setAliases(['stop'])
  .setStatus('beta')

exports.run = handler
exports.help = help
exports.main = leave

;
