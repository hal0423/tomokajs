'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')

// const LanguageDetect = require('languagedetect')
// const { languages } = require('google-translate-api')
const ggTrans = require('google-translate-api')
const googleTTS = require('google-tts-api')

const log = debug('bot:command:tts')
const errLog = debug('err:bot:command:tts')

const tts = async (text) => {
  const lang = await detect(text)
  // const code = languages.getCode(lang)
  log(lang)

  return googleTTS(text, code, 1)
}

const handler = (message, { content }) => 
  new Promise((resolve, reject) => {
    if (!content) {
      return resolve("Please enter something")
    }

    if (content.length > 200) {
      return resolve('The text is tooooo loooooong')
    }

    const { voiceConnection } = message.guild
    
    if (!voiceConnection) {
      return resolve("I'm not in any voice channel here")
    }

    if (voiceConnection.speaking) {
      return resolve("I have only one mouth, dude")
    }

    tts(content)
    .then(url => {
      log(url)
      const dispatcher = voiceConnection.playArbitraryInput(url)
      dispatcher.once('end', log)
      dispatcher.on('error', errLog)
      dispatcher.once('start', () => log('start speaking'))
    })
    .catch(reject)
  })

const help = Help.init('tts')
  .setDescription('google text to speech')
  .setSelfHandle(true)
  .beta()

// function detect(text) {
//   const lang = new LanguageDetect()

//   return lang.detect(text, 1)[0][0]
// }

async function detect(text) {
  let translation
  try {
    translation = await ggTrans(text)
  } catch (err) {
    errLog(err)
    throw err
  }

  log(translation)
  return translation.from.language.iso
}
exports.run = handler
exports.help = help
exports.main = tts

;
