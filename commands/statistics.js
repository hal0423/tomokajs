'use strict'

const debug = require('debug')
const { Statistics } = require('../utils/statistics.js')
const { Help } = require('../utils/help.js')

const log = debug('bot:command:statistics')
const errLog = debug('bot:err:command:statistics')

const dataParser = data => {
  if (!data) {
    return null
  }

  data = data
  .replace(/,/g, '.')
  .replace(/\ /g, '')
  .split('|')
  .map(v => Number(v))

  return data.includes(NaN)
    ? null
    : data
}

Array.prototype.dataToString = function() {
  
  return this.reduce((v, x, y, z) => {
    v += x
    
    return v + (z.length - 1 === y ? ']' : ', ')
  }, '[')

}

const statistics = (message, { content }) => 
  new Promise(async (resolve, reject) => {
    const data = dataParser(content)
    if (!data) {
      return reject('Please enter a valid data, remember to use `|` to seperate them')
    }

    const sta = new Statistics(data)

    const embed = {
      title: 'Statistics',
      description: `Data: ${sta.data.dataToString()}\nSum: ${sta.sumData}`,
      fields: [
        {
          name: 'x^2',
          value: `Data: ${sta.computed.dataPow.dataToString()}\nSum: ${sta.computed.sumDataPow}`,
          inline: false
        },
        {
          name: '1/x',
          value: `Data: ${sta.computed.dataDiv.dataToString()}\nSum: ${sta.computed.sumDataDiv}`,
          inline: false
        },
        {
          name: 'Statistical means',
          value: 'Arithmetic mean: '   + sta.arithmetic() +
                  '\nGeometric mean: ' + sta.geometric() +
                  '\nHarmonic mean: '  + sta.harmonic() +
                  '\nQuadratic mean: ' + sta.quadratic(),
          inline: false
        },
        {
          name: 'Deviation',
          value: 
            '  **d  :** '   + sta.absoluteDeviation() +
            '\n**σ^2:** ' + sta.squareDeviation() +
            '\n**σ  :** ' + sta.standardDeviation() +
            '\n**s^2:** ' + sta.squareDeviation(true) +
            '\n**s  :** ' + sta.standardDeviation(true),
          inline: false
        }
      ]
    }

    resolve({embed})
  })



const help = Help.init('statistics')
  .setAliases(['s', 'statistical'])
  .stable()

exports.run = statistics
exports.help = help


;
