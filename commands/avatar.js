'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')
const getColors = require('get-image-colors')

const log = debug('bot:command:avatar')
const errLog = debug('err:bot:command:avatar')

async function enchance(embed) {
  const colors = await getColors(embed.image.url)

  embed.color = colors[0].num()

  log(embed)

  return { embed }
}

const avatar = user => {
  const { displayAvatarURL, username, discriminator } = user
  const embed = {
    title: `${username}#${discriminator}`,
    timestamp: new Date(),
    image: {
      url: displayAvatarURL
    }
  }

  return { embed }
}

const handler = message => 
  new Promise((resolve, reject) => {
    const mentions = message.mentions.users

    const self = message.author
    const user = mentions && mentions.first()

    const content = avatar(user || self)
    const msg = message.channel.send(content)
    
    Promise.all([msg, enchance(content.embed)])
      .then(([mess, em]) => mess.edit(em))
      .then(resolve)
      .catch(err => {
        errLog(err)
        reject(err)
      })
  })

const help = Help.init('avatar')
  .setAliases(['ava'])
  .setDescription('Get the user\'s avatar')
  .setCountDown(8000)
  .setSelfHandle(true)
  .stable()

exports.run = handler
exports.help = help
exports.main = avatar

;
