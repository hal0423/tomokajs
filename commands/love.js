'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')
const { love } = require('./.commandInfo.json')

const log = debug('bot:command:love')
const errLog = debug('bot:err:command:love')


const cal = ({ n, m }) => Math.round((n > m ? m / n : n / m) * 100)

const meter = g => {
  let u = '',
      d = g / 5

  for (let v = 0; v < d; v++) { u += '█' }
  for (let v = 0; v < 19 - d; v++) { u += '░' }

  return u
}

const gen = (id, reverse) => {
  let a = id.toString().split('')
  
  if (reverse) {
    a = a.reverse()
  }

  return parseInt(a.slice(6).join())
}

const mess = r => {
  if (r === 100) return ':heartpulse: Hai con người này sinh ra là để dành cho nhau :heartpulse:'
  if (r >= 95) return 'Hai con người của định mệnh đây rồi :heartpulse:'
  if (r >= 80) return 'Duyên phận đã định :heart_eyes:'
  if (r >= 60) return 'Khá hợp nhau đấy, có triển vọng :kissing_heart:'
  if (r >= 40) return 'Giới giang hồ hiểm ác khó lường trước được điều gì'
  if (r >= 20) return 'Có vẻ thú vị'
  return 'Quay đầu là bờ '
}

function loveMeter(message) {
  return new Promise(async (resolve, reject) => {
    const users = message.mentions.users.first(2)

    const a = users[1] || message.author
    const b = users[0] || message.author

    const f = a > b
    const i = {
      n: gen(a.id, f),
      m: gen(b.id, !f)
    }

    const isLover = !!love.lover.filter(x => x.includes(a.id) && x.includes(b.id)).length
    
    const hasRandomRate = !!love.randomRate.filter(x => x.includes(a.id) && x.includes(b.id)).length

    const point = isLover
                  ? 100
                  : hasRandomRate
                    ? 100 * Math.random() << 0
                    : cal(i)


    const cong = Number(a.toString().slice(-1)) > Number(b.toString().slice(-1))
    const userA = cong ? a.username : b.username
    const userB = cong ? b.username : a.username

    const embed = {
      title: 'Thước đo tình yêu',
      description: `:sparkling_heart: **${userA}**\n:sparkling_heart: **${userB}**`,
      color: 16524136,
      thumbnail: { url: 'https://i.imgur.com/YpRPxeS.png' },
      fields: [
        {
          name: point + '%',
          value: meter(point),
          inline: false
        },
        {
          name: point + '%',
          value: users.length !== 0 ? mess(point) : 'Không có người yêu hay sao mà phải tự yêu bản thân thế...',
          inline: false
        }
      ]
    }

    log(`${userA} <3 ${userB} = ${point}%`)
    
    return resolve({embed})

  })

}

const help = Help.init('love')
  .setDescription('Love meter')
  .setUsage('%plove <tagSomeOne> <?tagSomeOne>')
  .setCountDown(2500)
  .stable()
  
exports.run = loveMeter
exports.help = help


;
