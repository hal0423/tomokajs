'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')

const log = debug('bot:command:send')
const errLog = debug('bot:err:command:send')

function send(message, { content }) {
  return new Promise(async (resolve, reject) => {
    if (!content && !message.attachments.size) {
      reject({message: 'Sorry but I can\'t send an emty thing'})
    }

    try {
    
      const mess = await message.channel.send(content)
      message.delete()
      resolve(mess)
      
    } catch (err) {
      errLog(err)
      reject({message: `Something went wrong!`})
    }    
  })

}

const help = Help.init('send')
  .setAliases(['say'])
  .setDescription('send something')
  .setUsage('%psend <things>')
  .setStatus('beta')
  .canHandle()

exports.run = send
exports.help = help


;
