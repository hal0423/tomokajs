'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')

const log = debug('bot:command:addRgb')
const errLog = debug('bot:err:command:addRgb')

const SERVER = require('./.commandInfo.json').rgb.server

const addRgb = message => new Promise(async (resolve, reject) => {
  const serverID = message.guild.id

  if (!SERVER.includes(serverID)) return reject()

  if (!message.mentions.members.size) {
    return reject('Please mention the person who wished to be a part of the RGB fanclub')
  }

  const rgbRoles = message.client.servers
    .get(serverID)
    .get('roleAliases')
    .get('rgb')

  for (let [_, member] of message.mentions.members) {
    try {
      const memberRoles = [...member.roles].map(v => v[0])
      const roles = [...rgbRoles].filter(v => !memberRoles.includes(v))
      
      await member.addRoles(roles)
    } catch (err) {
      errLog(err)
      return reject('something went wong')
    }

  }
  
  resolve('Done')
})




const help = Help.init('addrgb')
  .setPermission(['SEND_MESSAGES', 'MANAGE_ROLES'])
  .setUserPermission(['MANAGE_ROLES'])
  .useDB()
  .stable()

exports.run = addRgb
exports.help = help


;
