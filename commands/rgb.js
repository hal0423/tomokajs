'use strict'

const debug      = require('debug')
const { Server } = require('../db/models.js')
const { Help }   = require('../utils/help.js')

const log      = debug('bot:command:rgb')
const errLog   = debug('err:bot:command:rgb')
const errDBLog = debug('err:db:command:rgb')

const SERVER = require('./.commandInfo.json').rgb.server

const rgb = message => new Promise(async (resolve, reject) => {
  const serverID = message.guild.id

  if (!SERVER.includes(serverID)) {
    return reject()
  }

  const ser = message.client.servers
    .get(serverID)
    .get('roleAliases')
    .get('rgb')
  
  if (!message.mentions.roles.size) {
    return resolve(`RGB has ${ser.size} roles in total`)
  }

  const role = message.mentions.roles
    .filter(v => ser.has(v.id) || v.mentionable)
    .map(v => ({
      id: v.id,
      name: v.name,
      color: v.color
    }))

  log(role)

  if (!role.length) {
    return reject('Please enter a mentionable role')
  }

  try {
    const server = await Server.findOne({ id: serverID })
    const rgbIndex = server.roleAliases.findIndex(v => v.name === 'rgb')

    role.forEach(v => {
      server.roleAliases[rgbIndex].roles.push(v)
      ser.add(v.id)
    })
    
    server.markModified("roleAliases")
    server.save()
    .catch(errDBLog)

    return resolve(`OMG RGB HAS EARNED NEW ROLES AGAIN\n@rgb now has ${ser.size} roles in total`)

  } catch (err) {
    errLog(err)
    return reject({message: `Something went wrong!`})
  }

  
})


const help = Help.init('rgb')
  .setPermission(['SEND_MESSAGES', 'MANAGE_ROLES'])
  .setUserPermission(['MANAGE_ROLES'])
  .useDB()
  .stable()

exports.run = rgb
exports.help = help


;
