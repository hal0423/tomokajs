'use strict'

const debug = require('debug')
const { Info, Command } = require('../db/models.js')

const log = debug('bot:command:information')
const errLog = debug('bot:err:command:information')

function information(message, { content }) {
  return new Promise(async (resolve, reject) => {
    log("LoliChronicle")

    try {
    
      const mess = await message.channel.send()
      resolve(mess)
      
    } catch (err) {
    
      errLog(err)
      reject(`Something went wrong!`)
      
    }    
  })

}

const help = {
  name: 'information',
  aliases: ['info'],
  description: 'My information',
  usage: 'information',
  countDown: 2500,
  permission: ['SEND_MESSAGES'],
  status: 'development'
}

exports.run = information
exports.help = help


;
