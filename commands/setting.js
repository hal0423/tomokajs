'use strict'

const fs = require('fs').promises
const debug = require('debug')
const { Help } = require('../utils/help.js')

const log = debug('bot:command:set')
const errLog = debug('err:bot:command:set')

const cmdDir = './setting/'
const dbDir = './setting/db/'

const set = arg => {
  log("LoliChronicle")
  return 'Nothing yet!'
}

const handler = async (message, { content }) => {
  const [arg, ...value] = content.split(' ')

  const files = await fs.readdir(cmdDir)
  const commands = [...files].map(v => v.replace('.js', ''))

  if (!commands.includes(arg)) {
    log(`${arg} is not a setting command`)
    return `There are no setting for the ${arg} currently`
  }

  let mess

  try {
    mess = await require(cmdDir + arg + '.js')(message, value)
    
    if (message.client.useDb) {
      require(dbDir + arg + '.js')(message, value)
    }

  } catch (error) {
    errLog(error)
    throw error
  }

  return mess
}



const help = Help.init('setting')
  .setAliases(['set', 'option', 'opt'])
  .setDescription('')
  .setUsage('')

exports.run = handler
exports.help = help
exports.main = set

;
