'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')

const log = debug('bot:command:add')
const errLog = debug('err:bot:command:add')

const add = (voice, content) => {
  log("LoliChronicle")
  return 'Nothing yet!'
}

const handler = async (message, { content }) => {
  const info = add(message)
  return info
}

const help = Help.init('add')
  .setDescription('Add song')
  .setUsage('')

exports.run = handler
exports.help = help
exports.main = add

;
