'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')
const moment = require('moment')
moment().format()

const log = debug('bot:command:user')

const highlight = {
  top: ['Administrator'],
  first: ['Manage guild', 'Ban members', 'Kick members'],
  second: ['Manage messages', 'Manage nicknames', 'Manage roles or permissions', 'View audit log']
}

function formatPermission(str) {
  return str[0] + str.slice(1).toLowerCase().replace(/_/g, ' ')
}

Array.prototype.sortHighLight = function() {
  const list = [
    [], [], [], []
  ]
  
  for (const per of this) {
    highlight.top.includes(per) ? list[0].push(`__**${per}**__`)
    : highlight.first.includes(per) ? list[1].push(`**${per}**`)
    : highlight.second.includes(per) ? list[2].push(`__${per}__`)
    : list[3].push(per)
  }
  
  return [...list.map(v => v.sort())]
    .reduce((v, x) => [...v, ...x],[])
}

const user = member => {
  const { user, guild } = member
  
  const createdDay = moment(user.createdTimestamp)
  const joinedDay = moment(member.joinedTimestamp)
  
  const roles = member.roles
  .sort((a, b) => b.calculatedPosition - a.calculatedPosition)
  .map(v => v.name).filter(v => v !== "@everyone")
  
  const permissions = member.permissions.toArray()
  .map(formatPermission)
  .sortHighLight()
  .join(', ')
  
  let displayRoles = roles.slice(0, 15).join(', ')
  
  if (!roles.length) {
    displayRoles === `$__{member.displayName}__ doesn't have any role on *${guild.name}* yet`
  } else if (roles.length > 15) {
    displayRoles += `,... (and ${roles.length - 15} more)`
  }
  
  const embed = {
    author: {
      name: member.displayName,
      icon_url: user.displayAvatarURL
    },
    color: member.displayColor,
    timestamp: new Date(),
    footer: {
      icon_url: guild.iconURL,
      text: guild.name
    },
    thumbnail: {
      url: user.displayAvatarURL
    },
    fields: [
      {
        name: "Name",
        value: user.username,
        inline: true
      },
      {
        name: "Discriminator",
        value: user.discriminator,
        inline: true
      },
      {
        name: "Status",
        value: user.presence.status,
        inline: true
      },
      {
        name: "ID",
        value: user.id,
        inline: true
      },
      {
        name: "Created at",
        value: `${createdDay.format("dddd, Do MMMM YYYY, h:mm:ss a")}\n(${createdDay.fromNow()})`,
      },
      {
        name: `Joined ${guild.name} at`,
        value: `${joinedDay.format("dddd, Do MMMM YYYY, h:mm:ss a")}\n(${joinedDay.fromNow()})`,
      },
      {
        name: `Roles (${roles.length} in total except @everyone)`,
        value: displayRoles
      },
      {
        name: `Permissions on ${guild.name}`,
        value: permissions
      }
    ]
  }
  
  if (member.user.bot) {
    embed.author.name += ' [BOT]'
  }
  
  log(embed)
  return { embed }
}

const handler = async (message) => {
  const mentions = message.mentions.members
  
  const self = message.member
  const member = mentions && mentions.first()
  
  const info = user(member || self)
  return info
}

const help = Help.init('user')
.setDescription('check user information')
.setUsage('')
.stable()

exports.run = handler
exports.help = help
exports.main = user

;
