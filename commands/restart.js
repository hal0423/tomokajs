'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')

const log = debug('bot:command:restart')
const errLog = debug('bot:err:command:restart')

const restart = (message, { content }) => 
new Promise(async (resolve, reject) => {
  const client = message.client
  
  if (message.author.id !== client.authorID) {
    return reject({message: 'This command is for my dear master only'})
  }

  switch (content) {
    case 'commands':
    case 'command':
      client.loadCommand()
      break
    case 'sticker':
    case 'stickers':
      client.loadSticker()
      break
  }

  log('Reloaded %s succesfully', content)

  resolve(`Reset ${content} successfully!!!`)
})



const help = Help.init('restart')
  .setAliases(['reset', 'reload'])
  .setUserPermission('author')
  .stable()

exports.run = restart
exports.help = help


;
