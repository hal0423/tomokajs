'use strict'

const debug = require('debug')

const log = debug('bot:command:delete')
const errLog = debug('bot:err:command:delete')

function del(message, { content }) {

 log("LoliChronicle")

  message.channel.send()
  .catch(errLog)
}

const help = {
  name: 'delete',
  aliases: [],
  description: 'delete things',
  usage: 'delete <whatToDelete> <option>',
  countDown: 2500,
  permission: [],
  status: 'development'
}

exports.run = del
exports.help = help


;