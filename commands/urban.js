'use strict'

const debug = require('debug')
const request = require('request-promise')
const { Help } = require('../utils/help.js')

const log = debug('bot:command:urban')
const errLog = debug('bot:err:command:urban')

const urban = (message, { content }) => 
  new Promise((resolve, reject) => {
    return getUrban(content)
      .then(embed => {
        log(embed)
        resolve({embed})    
      })
      .catch(reject)
  })

async function getUrban(word) {
  const url = word === '' ? 'http://api.urbandictionary.com/v0/random' : `http://api.urbandictionary.com/v0/define?term=${word}`

  const re = await request({
    uri: url,
    json: true
  })

  if (re.result_type === 'no_results') {
    return `Em lục nát cái UrbanDictionary rồi mà không kiếm ra được **${word}** là gì...`
  }

  const g = re.list[word === '' ? Math.round(Math.random() * re.list.length) : re.list.reduce((m, x, i, arr) => x.thumbs_up > arr[m].thumbs_up ? i : m, 0)]

  const embed = {
    title: `Definition of ${g.word}`,
    description: g.definition,
    url: g.permalink,
    color: Math.round(Math.random() * 16777215),
    author: {name: g.author},
    timestamp: new Date(),
    fields:
      [
        {
          name: 'Example',
          value: g.example,
          inline: false
        },
        {
          name: ':thumbsup: ',
          value: g.thumbs_up,
          inline: true
        },
        {
          name: ':thumbsdown: ',
          value: g.thumbs_down,
          inline: true
        },
      ]
  }

  return embed

}



const help = Help.init('urban')
  .setAliases(['u'])
  .stable()

exports.run = urban
exports.help = help


;

//getUrban('loli').then(console.log)
