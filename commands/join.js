'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')

const log = debug('bot:command:join')

const handler = message => 
  new Promise((resolve, reject) => {
    const { client, channel, guild, member: { voiceChannel } } = message
    const { voiceConnection } = guild

    if (!voiceChannel) {
      return resolve("You must be in a voice channel for me to sit next to you")
    }

    if (voiceConnection) {
      return resolve(`I'm already in the **${voiceConnection.channel.name}**`)
    }

    if (voiceChannel.full) {
      return resolve("There are no space for me to stand in the channel...")
    }

    if (!voiceChannel.joinable) {
      return resolve("Cannot join the voice channel...")
    }

    if (!voiceChannel.speakable) {
      return resolve("I cannot sing in that channel....")
    }

    voiceChannel
      .join()
      .then(connnection => {
        const mess = `Joined **${connnection.channel.name}**`

        client.servers.get(guild.id).set('voice', {
          logChannel: channel.id,
          queue: [],
          currentIndex: 0,
          playing: false
        })
        
        log(`${mess} on server ${guild.name}`)

        resolve(mess)
      })
      .catch(reject)
  })



const help = Help.init('join')
  .setDescription('Join the same voice channel with you on this server')
  .setStatus('beta')

exports.run = handler
exports.help = help

;
