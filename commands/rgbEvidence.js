'use strict'

const debug = require('debug')
const fs = require('fs').promises
const path = require('path')
const { Help } = require('../utils/help.js')

const log = debug('bot:command:rgbEvidence')
const errLog = debug('err:bot:command:rgbEvidence')

async function getEvidenceList() {
  const { cache } = require('../bot.js')

  let list
  if (cache.has('rgbEvidence')) {
    list = cache.get('rgbEvidence')
  } else {
    const files = await fs.readdir(path.resolve('./img/RGB Evidence'))
    cache.set('rgbEvidence', files)
    list = files
  }
  
  return list
}

const rgbEvidence = async client => {
  if (!client.stuff.has('rgbEvidence')) {
    log(client.stuff)
    throw 'Doesn\'t have evidence'
  }
  
  const list = await getEvidenceList()
  return list[list.length * Math.random() << 0]
}

const handler = async message => {
  try {
    let info = await rgbEvidence(message.client)
    info = path.resolve('.', 'img', 'RGB Evidence', info)

    log(info)
    
    return { file: info }
  } catch (err) {
    errLog(err)
    throw err
  }

}



const help = Help.init('rgbEvidence')
  .setAliases(['evidence', 'evi'])
  .setDescription('Get random evidence of RGB')
  .setCountDown(120000)
  .allowDM()
  .stable()

exports.run = handler
exports.help = help
exports.main = rgbEvidence

;
