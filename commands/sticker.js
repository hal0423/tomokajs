'use strict'

const debug = require('debug')

const log = debug('bot:command:sticker')
const errLog = debug('err:bot:command:sticker')

const sticker = (message, { content }) => 
new Promise(async (resolve, reject) => {
  log("LoliChronicle")

  try {
    const mess = await message.channel.send()
    resolve(mess)
  } catch (err) {
    errLog(err)
    reject({message: `Something went wrong!`})
  }    
})



const help = {
  name: 'sticker',
  aliases: [],
  description: 'Sticker',
  usage: '',
  countDown: 4000,
  permission: ['SEND_MESSAGES'],
  userPermission: [],
  status: 'development'
}

exports.run = sticker
exports.help = help


;
