'use strict'

const mongoose = require('mongoose')
const db = require('./config/db.js')


exports.connect = address => {
  const options = db.options('tmokenc')

  return mongoose.connect(`mongodb://${address}`, options)
}

exports.disconnect = () => mongoose.disconnect()