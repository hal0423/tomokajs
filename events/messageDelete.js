'use strict'
const debug = require('debug')

const log = debug('bot:messageDelete')
const errLog = debug('bot:err:messageDelete')

module.exports = message => {
  if (message.author.bot) return

  const logChannel = message.guild.channels.find(v => v.name === 'server-log')

  if (!logChannel) return

  const mess = `A message by **${message.author.username}** on channel ${message.channel} has been deleted`

  const embed = {
    timestamp: new Date(),
    color: 0xdb5f4e
  }

  if (message.attachments) {
    let image = message.attachments.first()
    if (image) embed.image = {url: image.url}
  } 

  if (message.content) {
    embed.fields =  [
          {
            name: 'Deteled message',
            value: message.content
          }
        ]
  }

  log(`${embed.timestamp}: ${mess}`)
  
  logChannel.send(mess, {embed})
  .catch(errLog)
}

    //Emitted whenever a message is deleted.

    //Params

    //The deleted message 
