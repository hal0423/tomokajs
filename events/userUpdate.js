'use strict'

const { diff } = require('deep-diff')
  , debug = require('debug')('bot:userUpdate')

    //Emitted whenever a user's details (e.g. username) are changed.

    //Params

    //The user before the update 
    //The user after the update 
module.exports = (oldUser, newUser) => {
  const change = diff(oldUser, newUser)

  if (change.kind !== 'E') return
  change.map.forEach(v => {
    const string = `${oldUser.username} has change ${v.path[v.path.length - 1]} from ${e.lhs} to ${e.rhs}`
    debug(string)
    oldUser.client.channels.find('name', 'server-log').send(string)
    
  })
}