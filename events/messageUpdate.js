'use strict'
const debug = require('debug')('bot:messageUpdate')

module.exports = (oldMessage, newMessage) => {
  if (oldMessage.content === newMessage.content || oldMessage.author.bot || oldMessage.guild.id !== "418811018244784129") return
  
  const logChannel = oldMessage.guild.channels.find(v => v.name === 'server-log')
  if (!logChannel) return
  
  const message =  `The message by **${oldMessage.author.username}** on channel ${oldMessage.channel} has been edited`
  
  const embed = {
    timestamp: new Date(),
    color: 0x4edb5f,
    fields: [
      {
        name: 'Original message',
        value: oldMessage.content
      },
      {
        name: 'New message',
        value: newMessage.content
      }
    ]
  }
  
  debug(`${embed.timestamp}: ${message}`)
  
  logChannel.send(message, {embed})
}


//Emitted whenever a message is updated - e.g. embed or content change.

//Params

//The message before the update 
//The message after the update 

