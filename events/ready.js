const debug = require('debug')
const { Collection } = require('discord.js')

const log = debug('bot:ready')

const handler = client => {
  //console.log('\033c') // clear the terminal

	log('%s is online!', client.user.username)
  client.user.setActivity('Tmokenc#0001', { type: 'LISTENING' })
  
  for (const serverID of client.guilds.keys()) {
    client.servers.set(serverID, new Collection())
  }

  client.loadPrefix()
  if (client.useDb) {
    client.loadRoleAliases()
  }
  //require('../utils/update.js')(client)
  //require('../utils/addRgb.js')(client)
}

//Emitted when the client becomes ready to start working.

//Params

//has no parameters

module.exports = handler


;