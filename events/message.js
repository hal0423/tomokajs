'use strict'

const debug = require('debug')

const log = debug('bot:message')
const errlog = debug('err:bot:message')

const process = require('../utils/processMessage.js')

const highlight = ['tý', 'tí', 'nếu', 'chắc', 'một chút']

const handler = message => {
  if (message.author.bot) {
    return
  }
  
  message = process(message)
  const { content, guild, client } = message
  
  switch (message.channel.type) {
    case 'text':
      log(`${guild.name} #${message.channel.name} @${message.author.username}:\n${content}`)
      const serverInfo = client.servers.get(guild.id)
  
      if (serverInfo) {
        message.processRoleAliases(serverInfo.get('roleAliases'))
      }
      break
    
    case 'dm':
      log(`A DM from ${message.author.username}:\n${message.content}`)
      break
  }
  
  if (message.content.split(' ')[0].toLowerCase() === 'f') {
    const { main: f } = require('../commands/respect')
    const content = message.content.slice(2)
    
    message.channel.send(f(message, content))
    return
  }
  
  const matchedPrefix = message.matchPrefix()
  
  if (!matchedPrefix) {
    const matchHighlights = message.matchWords(highlight)
    
    if (matchHighlights.length) {
      const logHighlight = debug("bot:message:highlight")
      const highlights = matchHighlights.join(', ')
      
      logHighlight(highlights)
      message.channel.send(`**${highlights}**`)
    }
    
    return
  }
  
  const command = content.slice(matchedPrefix.length).split(' ')[0].toLowerCase()
  
  message.processCommand(matchedPrefix, command, message)
  .catch(err => {
    errlog(err)
    
    if (message.usingAsTester()) {
      message.channel.send("```js\n"+ err +"```")
      return
    }
    
    if (typeof err === 'string') {
      message.channel.send(err)
    } else if (err.message) {
      message.channel.send(err.message)
    }
    
  })
  
}

module.exports = handler


;